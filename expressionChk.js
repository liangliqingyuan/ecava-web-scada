// To try reproduce Tag Expression problem found in Georg Jordan Project
var list = ["CJR_TARGET_KG_8","CJR_TARGET_KG_7","CJR_TARGET_KG_6","CJR_TARGET_KG_5","CJR_TARGET_KG_4","CJR_TARGET_KG_3","CJR_TARGET_KG_2","CJR_TARGET_KG_1",
"CJH_TARGET_KG_8","CJH_TARGET_KG_7","CJH_TARGET_KG_6","CJH_TARGET_KG_5","CJH_TARGET_KG_4","CJH_TARGET_KG_3","CJH_TARGET_KG_2","CJH_TARGET_KG_1",
"R_ACTUAL_KG_8","R_ACTUAL_KG_7","R_ACTUAL_KG_6","R_ACTUAL_KG_5","R_ACTUAL_KG_4","R_ACTUAL_KG_3","R_ACTUAL_KG_2","R_ACTUAL_KG_1",
"H_ACTUAL_KG_8","H_ACTUAL_KG_7","H_ACTUAL_KG_6","H_ACTUAL_KG_5","H_ACTUAL_KG_4","H_ACTUAL_KG_3","H_ACTUAL_KG_2","H_ACTUAL_KG_1"];

for (var i in list) {
    var data = getTag(list[i]) || Math.random()*200;
	data = getProcessData( data, 200);
	setTag( list[i] , data);
}

function getProcessData( prev, range) {
    var val = Math.random()*range;
	if ((prev + val)> range) {
	    rnd = prev - val;
	    if (0 > rnd) { rnd = val/10;}
	}
	else {
	    rnd = prev + val;
    }
    return rnd;
}
sleep(1000);
setTag( 'R_TOTAL_TARGET_KG_Chk', getTag('CJR_TARGET_KG_8') + getTag('CJR_TARGET_KG_7') + getTag('CJR_TARGET_KG_6') + getTag('CJR_TARGET_KG_5') + getTag('CJR_TARGET_KG_4') + getTag('CJR_TARGET_KG_3') + getTag('CJR_TARGET_KG_2') + getTag('CJR_TARGET_KG_1'));

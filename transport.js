
/***** Transportation-MOXA code start *****/
//Kelana Jaya Line
var tren = [];
if (getTag("tren1_run") == 1) { tren.push("tren1_");}
if (getTag("tren2_run") == 1) { tren.push("tren2_");}
if (getTag("tren3_run") == 1) { tren.push("tren3_");}
if (getTag("tren4_run") == 1) { tren.push("tren4_");}
if (getTag("tren5_run") == 1) { tren.push("tren5_");}
if (getTag("tren6_run") == 1) { tren.push("tren6_");}
for (var i in tren) {
	var counter = +(getTag(tren[i]+'counter')) || 0;
	var reverse = +(getTag(tren[i]+'reverse')) || 0;
	if ((counter == 0)||(counter == 120)||(counter == 215)||(counter == 315)||(counter == 415)||(counter == 510)||(counter == 590)||(counter == 680)||(counter == 795)||(counter == 875)||(counter == 955)||(counter == 1115)||(counter == 1335)||(counter == 1390)||(counter == 1470)||(counter == 1550)||	(counter == 1620)||(counter == 1715)||(counter == 1805)||(counter == 1900)||(counter == 1995)||(counter == 2085)||(counter == 2180)||(counter == 2300)) {
		var pause = +(getTag(tren[i]+'pause')) || 0;
		pause++;
		if (pause > 10) {
			if (reverse == 0) {
				counter++;
			} else {
				counter--;
			}
		}
		setTag(tren[i]+'pause', pause);
	} else {
		setTag(tren[i]+'pause', 0);
		if (reverse == 0) {
			counter++;
		} else {
			counter--;
		}
	}
	setTag(tren[i]+'counter', counter);
	if (counter >= 2300) {
		setTag (tren[i]+'reverse', 1);
	}
	if (counter < 0) {
		setTag (tren[i]+'reverse', 0);
	}
	if (counter < 600) {
		setTag(tren[i]+'A', 1);
		setTag(tren[i]+'B', 0);
		setTag(tren[i]+'C', 0);
		setTag(tren[i]+'D', 0);
		setTag(tren[i]+'E', 0);
		setTag(tren[i]+'F', 0);
	} else if (counter < 800) {
		setTag(tren[i]+'A', 0);
		setTag(tren[i]+'B', 1);
		setTag(tren[i]+'C', 0);
		setTag(tren[i]+'D', 0);
		setTag(tren[i]+'E', 0);
		setTag(tren[i]+'F', 0);
	} else if (counter < 1010) {
		setTag(tren[i]+'A', 0);
		setTag(tren[i]+'B', 0);
		setTag(tren[i]+'C', 1);
		setTag(tren[i]+'D', 0);
		setTag(tren[i]+'E', 0);
		setTag(tren[i]+'F', 0);
	} else if (counter < 1080) {
		setTag(tren[i]+'A', 0);
		setTag(tren[i]+'B', 0);
		setTag(tren[i]+'C', 0);
		setTag(tren[i]+'D', 1);
		setTag(tren[i]+'E', 0);
		setTag(tren[i]+'F', 0);
	} else if (counter < 1600) {
		setTag(tren[i]+'A', 0);
		setTag(tren[i]+'B', 0);
		setTag(tren[i]+'C', 0);
		setTag(tren[i]+'D', 0);
		setTag(tren[i]+'E', 1);
		setTag(tren[i]+'F', 0);
	} else {
		setTag(tren[i]+'A', 0);
		setTag(tren[i]+'B', 0);
		setTag(tren[i]+'C', 0);
		setTag(tren[i]+'D', 0);
		setTag(tren[i]+'E', 0);
		setTag(tren[i]+'F', 1);
	}
}
if (getTag('tren1_counter') == 460) { setTag('tren3_run', 1);}
if (getTag('tren2_counter') == 1840) { setTag('tren4_run', 1);}
if (getTag('tren3_counter') == 460) { setTag('tren5_run', 1);}
if (getTag('tren4_counter') == 1840) { setTag('tren6_run', 1);}
// Ampang Line - 1
var trenA = [];
if (getTag("tren7_run") == 1) { trenA.push("tren7_");}
if (getTag("tren8_run") == 1) { trenA.push("tren8_");}
if (getTag("tren9_run") == 1) { trenA.push("tren9_");}
for (var j in trenA) {
	var counterA = +(getTag(trenA[j]+'counter')) || 0;
	var reverseA = +(getTag(trenA[j]+'reverse')) || 0;
	if ((counterA == 0)||(counterA == 205)||(counterA == 390)||(counterA == 570)||(counterA == 780)||(counterA == 960)||(counterA == 1140)||(counterA == 1400)) {
		var pauseA = +(getTag(trenA[j]+'pause')) || 0;
		pauseA++;
		if (pauseA > 20) {
			if (reverseA == 0) {
				counterA++;
			} else {
				counterA--;
			}
		}
		setTag(trenA[j]+'pause', pauseA);
	} else {
		setTag(trenA[j]+'pause', 0);
		if (reverseA == 0) {
			counterA++;
		} else {
			counterA--;
		}
	}
	setTag(trenA[j]+'counter', counterA);
	if (counterA >= 1400) {
		setTag (trenA[j]+'reverse', 1);
	}
	if (counterA < 0) {
		setTag (trenA[j]+'reverse', 0);
	}
	if (counterA < 200) {
		setTag(trenA[j]+'A', 1);
		setTag(trenA[j]+'B', 0);
	} else {
		setTag(trenA[j]+'A', 0);
		setTag(trenA[j]+'B', 1);
	}
}
if (getTag('tren7_counter') == 700) { setTag('tren8_run', 1);}
// Ampang Line - 2
var trenB = [];
if (getTag("tren10_run") == 1) { trenB.push("tren10_");}
if (getTag("tren11_run") == 1) { trenB.push("tren11_");}
if (getTag("tren12_run") == 1) { trenB.push("tren12_");}
if (getTag("tren13_run") == 1) { trenB.push("tren13_");}
if (getTag("tren14_run") == 1) { trenB.push("tren14_");}
if (getTag("tren15_run") == 1) { trenB.push("tren15_");}

for (var j in trenB) {
	var counterB = +(getTag(trenB[j]+'counter')) || 0;
	var reverseB = +(getTag(trenB[j]+'reverse')) || 0;
	if ((counterB == 0)||(counterB == 205)||(counterB == 370)||(counterB == 490)||(counterB == 750)||(counterB == 1175)||(counterB == 1385)||(counterB == 1550)||(counterB == 1770)||(counterB == 1990)||(counterB == 2200)||(counterB == 2400)||(counterB == 2625)||(counterB == 2820)||(counterB == 2980)||(counterB == 3175)||(counterB == 3400)) {
		var pauseB = +(getTag(trenB[j]+'pause')) || 0;
		pauseB++;
		if (pauseB > 20) {
			if (reverseB == 0) {
				counterB++;
			} else {
				counterB--;
			}
		}
		setTag(trenB[j]+'pause', pauseA);
	} else {
		setTag(trenB[j]+'pause', 0);
		if (reverseB == 0) {
			counterB++;
		} else {
			counterB--;
		}
	}
	setTag(trenB[j]+'counter', counterB);
	if (counterB >= 3400) {
		setTag (trenB[j]+'reverse', 1);
	}
	if (counterB < 0) {
		setTag (trenB[j]+'reverse', 0);
	}
	if (counterB < 200) {
		setTag(trenB[j]+'A', 1);
		setTag(trenB[j]+'B', 0);
		setTag(trenB[j]+'C', 0);
		setTag(trenB[j]+'D', 0);
		setTag(trenB[j]+'E', 0);
	} else if (counterB < 800) {
		setTag(trenB[j]+'A', 0);
		setTag(trenB[j]+'B', 1);
		setTag(trenB[j]+'C', 0);
		setTag(trenB[j]+'D', 0);
		setTag(trenB[j]+'E', 0);
	} else if (counterB < 1350) {
		setTag(trenB[j]+'A', 0);
		setTag(trenB[j]+'B', 0);
		setTag(trenB[j]+'C', 1);
		setTag(trenB[j]+'D', 0);
		setTag(trenB[j]+'E', 0);
	} else if (counterB < 2200) {
		setTag(trenB[j]+'A', 0);
		setTag(trenB[j]+'B', 0);
		setTag(trenB[j]+'C', 0);
		setTag(trenB[j]+'D', 1);
		setTag(trenB[j]+'E', 0);
	} else {
		setTag(trenB[j]+'A', 0);
		setTag(trenB[j]+'B', 0);
		setTag(trenB[j]+'C', 0);
		setTag(trenB[j]+'D', 0);
		setTag(trenB[j]+'E', 1);
	}
}
if (getTag('tren10_counter') == 680) { setTag('tren12_run', 1);}
if (getTag('tren11_counter') == 2720) { setTag('tren13_run', 1);}
if (getTag('tren12_counter') == 680) { setTag('tren14_run', 1);}
if (getTag('tren13_counter') == 2720) { setTag('tren15_run', 1);}
//Monorail
var trenM = [];
if (getTag("tren16_run") == 1) { trenM.push("tren16_");}
if (getTag("tren17_run") == 1) { trenM.push("tren17_");}
for (var i in trenM) {
	var counter = +(getTag(trenM[i]+'counter')) || 0;
	var reverse = +(getTag(trenM[i]+'reverse')) || 0;
	if ((counter == 50)||(counter == 280)||(counter == 390)||(counter == 600)||(counter == 720)||(counter == 800)||(counter == 930)||(counter == 1130)||(counter == 1300)||(counter == 1630)||(counter == 1800)) {
		var pause = +(getTag(trenM[i]+'pause')) || 0;
		pause++;
		if (pause > 20) {
			if (reverse == 0) {
				counter++;
			} else {
				counter--;
			}
		}
		setTag(trenM[i]+'pause', pause);
	} else {
		setTag(trenM[i]+'pause', 0);
		if (reverse == 0) {
			counter++;
		} else {
			counter--;
		}
	}
	setTag(trenM[i]+'counter', counter);
	if (counter >= 1800) {
		setTag (trenM[i]+'reverse', 1);
	}
	if (counter < 0) {
		setTag (trenM[i]+'reverse', 0);
	}
	if (counter < 180) {
		setTag(trenM[i]+'A', 1);
		setTag(trenM[i]+'B', 0);
		setTag(trenM[i]+'C', 0);
		setTag(trenM[i]+'D', 0);
		setTag(trenM[i]+'E', 0);
		setTag(trenM[i]+'F', 0);
	} else if (counter < 360) {
		setTag(trenM[i]+'A', 0);
		setTag(trenM[i]+'B', 1);
		setTag(trenM[i]+'C', 0);
		setTag(trenM[i]+'D', 0);
		setTag(trenM[i]+'E', 0);
		setTag(trenM[i]+'F', 0);
	} else if (counter < 500) {
		setTag(trenM[i]+'A', 0);
		setTag(trenM[i]+'B', 0);
		setTag(trenM[i]+'C', 1);
		setTag(trenM[i]+'D', 0);
		setTag(trenM[i]+'E', 0);
		setTag(trenM[i]+'F', 0);
	} else if (counter < 800) {
		setTag(trenM[i]+'A', 0);
		setTag(trenM[i]+'B', 0);
		setTag(trenM[i]+'C', 0);
		setTag(trenM[i]+'D', 1);
		setTag(trenM[i]+'E', 0);
		setTag(trenM[i]+'F', 0);
	} else if (counter < 1500) {
		setTag(trenM[i]+'A', 0);
		setTag(trenM[i]+'B', 0);
		setTag(trenM[i]+'C', 0);
		setTag(trenM[i]+'D', 0);
		setTag(trenM[i]+'E', 1);
		setTag(trenM[i]+'F', 0);
	} else {
		setTag(trenM[i]+'A', 0);
		setTag(trenM[i]+'B', 0);
		setTag(trenM[i]+'C', 0);
		setTag(trenM[i]+'D', 0);
		setTag(trenM[i]+'E', 0);
		setTag(trenM[i]+'F', 1);
	}
}
/***** Transportation-MOXA code end *****/
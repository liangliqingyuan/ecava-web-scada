var hour = getTag("app.currentTime.hour");

if (hour == 0 || hour == 8 || hour == 16) setTag("email_trigger01", hour);
else if (hour == 4 || hour == 12 || hour == 20) setTag("email_trigger02", hour);
else setTag("email_trigger01", hour);
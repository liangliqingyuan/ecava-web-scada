
/***** Server Starup Alert & Modsim Write code start *****/

var ctr = getTag( 'svrStartd');
ctr = parseFloat( ctr);
if (isNaN( ctr)) {
	ctr = 0;
}
setTag( 'svrStartd', ++ctr);

var alrmDelay = getTag('alrmDelay');

var wmi = new ActiveXObject('WbemScripting.SWbemLocator');
var wsh = new ActiveXObject("WScript.shell"); 
var wmiInstance = wmi.ConnectServer('.', "root\\cimv2");
var Processes = wmiInstance.ExecQuery("Select * from Win32_Process Where Name = 'Mod_RSsim.exe'");

if (getTag('device.MODSIM.status.connected')) {
	if (60 <= ctr) {
		var modsim = {
			'HR40001':30000,
			'HR40002':30000,
			'HR40003':30000
		};
		for (var i in modsim) {
    		var data = getTag(i)
    		if ((data == 0)||(isNaN(data))) {
				data = parseInt(Math.random()*modsim[i]);
			} else {
				data = getProcessData( data, modsim[i]);
			}
			setTag( i , data);
		}
		setTag( 'svrStartd', alrmDelay);
	}
} else {
	if (alrmDelay+10 < ctr) {
		//.run("file:///C:\\Users\\Administrator\\Desktop\\mod_RSsimEXE\\mod_RSsim.exe");
		setTag( 'svrStartd', alrmDelay);
	}
}

if (Processes.Count >= 2) {
	//wsh.run("taskkill.exe /im Mod_RSsim.exe");
}
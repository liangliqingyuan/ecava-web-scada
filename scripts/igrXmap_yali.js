

	var map = new BMap.Map("allmap");
	   map.addControl(new BMap.MapTypeControl({mapTypes: [BMAP_NORMAL_MAP,BMAP_HYBRID_MAP]})); 

	
	
	map.centerAndZoom(new BMap.Point(104.954743,33.123796), 5);
	var myIcon1 = new BMap.Icon("images/yali_h.png", new BMap.Size(30,30));
	var myIcon2 = new BMap.Icon("images/yahi_l.png", new BMap.Size(30,30));
	var myIcon3 = new BMap.Icon("images/yali_n.png", new BMap.Size(30,30));	
    var opts = {
	  width : 150,    
	  height: 250,    
	}	
	var RG02_PI101 = 0.5;
	var RG02_PI201 = 0.8;
	var RG02_PI301 = 1.0;
	var RG02_PI401 = 1.2;
	var RG02_PI101H = 0.55;
	var RG02_PI201H = 0.85;
	var RG02_PI301H = 1.05;
	var RG02_PI401H = 1.25;
	var RG02_PI101L = 0.45;
	var RG02_PI201L = 0.75;
	var RG02_PI301L = 0.95;
	var RG02_PI401L = 1.15;
       var myIcon02 = myIcon3;
	if (RG02_PI101 > RG02_PI101H || RG02_PI201 > RG02_PI201H || RG02_PI301 > RG02_PI301H || RG02_PI401 > RG02_PI401H){myIcon02 = myIcon1;} 
	else if (RG02_PI101 < RG02_PI101L || RG02_PI201 < RG02_PI201L || RG02_PI301 < RG02_PI301L || RG02_PI401 < RG02_PI401L){myIcon02 = myIcon2;}
	else {myIcon02 = myIcon3;}
	var point02 = new BMap.Point(121.486863,31.214189);
	var marker02 = new BMap.Marker(point02,{icon:myIcon02});  
	map.addOverlay(marker02);             
	var label02 = new BMap.Label("上海",{offset:new BMap.Size(20,-10)});
	marker02.setLabel(label02);	
	marker02.addEventListener("click", function(){ 
	var RG02_PI101 = 0.5;
	var RG02_PI201 = 0.8;
	var RG02_PI301 = 1.0;
	var RG02_PI401 = 1.2;
	var infoWindow02 = new BMap.InfoWindow("低区压力："+RG02_PI101+" mpa<br/>低区压力上限："+RG02_PI101H+" mpa <br/>低区压力下限："+RG02_PI101L+" mpa<br/>    中区压力："+RG02_PI201+" mpa <br/>中区压力上限："+RG02_PI201H+" mpa<br/>中区压力下限："+RG02_PI201L+" mpa<br/>    高区压力："+RG02_PI301+" mpa <br/>高区压力上限："+RG02_PI301H+" mpa <br/>高区压力下限："+RG02_PI301L+" mpa<br/>    超高区压力："+RG02_PI401+" mpa <br/>超高区压力上限："+RG02_PI401H+" mpa <br/>超高区压力下限："+RG02_PI401L+" mpa",opts);  // 创建信息窗口对        
		map.openInfoWindow(infoWindow02,point02); 
	});

	var RG03_PI101 = 0.5;
	var RG03_PI201 = 0.6;
	var RG03_PI301 = 0.7;
	var RG03_PI101L = 0.45;
	var RG03_PI201L = 0.55;
	var RG03_PI301L = 0.65;
	var RG03_PI101H = 0.55;
	var RG03_PI201H = 0.65;
	var RG03_PI301H = 0.75;   
    var myIcon03 = myIcon3;
	if (RG03_PI101 > RG03_PI101H || RG03_PI201 > RG03_PI201H || RG03_PI301 > RG03_PI301H){myIcon03 = myIcon1;} 
	else if (RG03_PI101 < RG03_PI101L || RG03_PI201 <  RG03_PI201L || RG03_PI301 <  RG03_PI301L){myIcon03 = myIcon2;}
	else {myIcon02 = myIcon3;}
	var point03 = new BMap.Point(115.404407,40.114728);
	var marker03 = new BMap.Marker(point03,{icon:myIcon03}); 
	map.addOverlay(marker03);            
	var label03 = new BMap.Label("北京",{offset:new BMap.Size(20,-10)});
	marker03.setLabel(label03);

	marker03.addEventListener("click", function(){   
	var RG03_PI101 = 0.5;
	var RG03_PI201 = 0.6;
	var RG03_PI301 = 0.7;
	var infoWindow03 = new BMap.InfoWindow("压力：   <br />  低区压力："+RG03_PI101+" mpa <br />低区压力上限："+RG03_PI101H+" mpa <br />低区压力下限："+RG03_PI101L+" mpa <br />    中区压力："+RG03_PI201+" mpa  <br />中区压力上限："+RG03_PI201H+" mpa<br />中区压力下限："+RG03_PI201L+" mpa<br />    高区压力："+RG03_PI301+" mpa <br />高区压力上限："+RG03_PI301H+" mpa <br />高区压力下限："+RG03_PI301L+" mpa ",opts);  // 创建信息窗口对象      
		map.openInfoWindow(infoWindow03,point03); 	});	
	
	
	var RG04_PI101 = 0.5;
	var RG04_PI201 = 0.86;
	var RG04_PI101L = 0.45;
	var RG04_PI201L = 0.8;
	var RG04_PI101H = 0.55; 
	var RG04_PI201H = 0.85;
    var myIcon04 = myIcon3;
	if (RG04_PI101 > RG04_PI101H || RG04_PI201 > RG04_PI201H){myIcon04 = myIcon1;} 
	else if (RG04_PI101 < RG04_PI101L || RG04_PI201 <  RG04_PI201L){myIcon04 = myIcon2;}
	else {myIcon02 = myIcon3;}
	var point04 = new BMap.Point(112.240072,23.231218);
	var marker04 = new BMap.Marker(point04,{icon:myIcon04}); 
	map.addOverlay(marker04);             
	var label04 = new BMap.Label("广州",{offset:new BMap.Size(20,-10)});
	marker04.setLabel(label04);
	
	
	marker04.addEventListener("click", function(){   
	var RG04_PI101 = 0.5;
	var RG04_PI201 = 0.86;
	var infoWindow04 = new BMap.InfoWindow("压力：   <br />  低区压力："+RG04_PI101+" mpa <br />低区压力上限："+RG04_PI101H+" mpa <br />低区压力下限："+RG04_PI101L+" mpa <br />    高区压力："+RG04_PI201+" mpa  <br />高区压力上限："+RG04_PI201H+" mpa<br />高区压力下限："+RG04_PI201L+" mpa ",opts);  // 创建信息窗口对象      
		map.openInfoWindow(infoWindow04,point04);
	});	
	
	
	var RG05_PI101 = 1.0; 
	var RG05_PI201 = 1.5;
	var RG05_PI301 = 1.8;
	var RG05_PI101H = 1.2;
	var RG05_PI201H = 1.6;
	var RG05_PI301H = 1.9;
	var RG05_PI101L = 1.1; 
	var RG05_PI201L = 1.4;
	var RG05_PI301L = 1.7;	
       var myIcon05 = myIcon3;
	if (RG05_PI101 > RG05_PI101H || RG05_PI201 > RG05_PI201H || RG05_PI301 > RG05_PI301H){myIcon05 = myIcon1;} 
	else if (RG05_PI101 < RG05_PI101L || RG05_PI201 <  RG05_PI201L || RG05_PI301 <  RG05_PI301L){myIcon05 = myIcon2;}
	else {myIcon02 = myIcon3;}
	var point05 = new BMap.Point(79.963577,37.128733);
	var marker05 = new BMap.Marker(point05,{icon:myIcon05}); 
	map.addOverlay(marker05);            
	var label05 = new BMap.Label("和田",{offset:new BMap.Size(20,-10)});
	marker05.setLabel(label05);
	
	
	marker05.addEventListener("click", function(){   
	var RG05_PI101 = 1.0;
	var RG05_PI201 = 1.5;
	var RG05_PI301 = 1.8;
	var infoWindow05 = new BMap.InfoWindow("压力：   <br />  低区压力："+RG05_PI101+" mpa <br />低区压力上限："+RG05_PI101H+" mpa <br />低区压力下限："+RG05_PI101L+" mpa <br />    中区压力："+RG05_PI201+" mpa  <br />中区压力上限："+RG05_PI201H+" mpa<br />中区压力下限："+RG05_PI201L+" mpa<br />    高区压力："+RG05_PI301+" mpa <br />高区压力上限："+RG05_PI301H+" mpa <br />高区压力下限："+RG05_PI301L+" mpa ",opts);  // 创建信息窗口对象      
		map.openInfoWindow(infoWindow05,point05); 
	});				
	
		var RG12_PI101 = 0.5;
	var RG12_PI201 = 0.8;
	var RG12_PI301 = 1.0;
	var RG12_PI401 = 1.2;
	var RG12_PI101H = 0.55;
	var RG12_PI201H = 0.85;
	var RG12_PI301H = 1.15;
	var RG12_PI401H = 1.25;
	var RG12_PI101L = 0.45;
	var RG12_PI201L = 0.75;
	var RG12_PI301L = 0.95;
	var RG12_PI401L = 1.15;
       var myIcon12 = myIcon3;
	if (RG12_PI101 > RG12_PI101H || RG12_PI201 > RG12_PI201H || RG12_PI301 > RG12_PI301H || RG12_PI401 > RG12_PI401H){myIcon12 = myIcon1;} 
	else if (RG12_PI101 < RG12_PI101L || RG12_PI201 < RG12_PI201L || RG12_PI301 < RG12_PI301L || RG12_PI401 < RG12_PI401L){myIcon12 = myIcon2;}
	else {myIcon12 = myIcon3;}
	var point12 = new BMap.Point(107.824721,34.718828);
	var marker12 = new BMap.Marker(point12,{icon:myIcon12});  
	map.addOverlay(marker12);             
	var label12 = new BMap.Label("西安",{offset:new BMap.Size(20,-10)});
	marker12.setLabel(label12);	
	marker12.addEventListener("click", function(){ 
	var RG12_PI101 = 0.5;
	var RG12_PI201 = 0.8;
	var RG12_PI301 = 1.0;
	var RG12_PI401 = 1.2;
	var infoWindow12 = new BMap.InfoWindow("低区压力："+RG12_PI101+" mpa<br/>低区压力上限："+RG12_PI101H+" mpa <br/>低区压力下限："+RG12_PI101L+" mpa<br/>    中区压力："+RG12_PI201+" mpa <br/>中区压力上限："+RG12_PI201H+" mpa<br/>中区压力下限："+RG12_PI201L+" mpa<br/>    高区压力："+RG12_PI301+" mpa <br/>高区压力上限："+RG12_PI301H+" mpa <br/>高区压力下限："+RG12_PI301L+" mpa<br/>    超高区压力："+RG12_PI401+" mpa <br/>超高区压力上限："+RG12_PI401H+" mpa <br/>超高区压力下限："+RG12_PI401L+" mpa",opts);  // 创建信息窗口对        
		map.openInfoWindow(infoWindow12,point12); 
	});

	var RG13_PI101 = 0.5;
	var RG13_PI201 = 0.6;
	var RG13_PI301 = 0.7;
	var RG13_PI101L = 0.45;
	var RG13_PI201L = 0.55;
	var RG13_PI301L = 0.65;
	var RG13_PI101H = 0.55;
	var RG13_PI201H = 0.65;
	var RG13_PI301H = 0.75;   
    var myIcon13 = myIcon3;
	if (RG13_PI101 > RG13_PI101H || RG13_PI201 > RG13_PI201H || RG13_PI301 > RG13_PI301H){myIcon13 = myIcon1;} 
	else if (RG13_PI101 < RG13_PI101L || RG13_PI201 <  RG13_PI201L || RG13_PI301 <  RG13_PI301L){myIcon13 = myIcon2;}
	else {myIcon12 = myIcon3;}
	var point13 = new BMap.Point(93.327651,29.780948);
	var marker13 = new BMap.Marker(point13,{icon:myIcon13}); 
	map.addOverlay(marker13);            
	var label13 = new BMap.Label("拉萨",{offset:new BMap.Size(20,-10)});
	marker13.setLabel(label13);

	marker13.addEventListener("click", function(){   
	var RG13_PI101 = 0.5;
	var RG13_PI201 = 0.6;
	var RG13_PI301 = 0.7;
	var infoWindow13 = new BMap.InfoWindow("压力：   <br />  低区压力："+RG13_PI101+" mpa <br />低区压力上限："+RG13_PI101H+" mpa <br />低区压力下限："+RG13_PI101L+" mpa <br />    中区压力："+RG13_PI201+" mpa  <br />中区压力上限："+RG13_PI201H+" mpa<br />中区压力下限："+RG13_PI201L+" mpa<br />    高区压力："+RG13_PI301+" mpa <br />高区压力上限："+RG13_PI301H+" mpa <br />高区压力下限："+RG13_PI301L+" mpa ",opts);  // 创建信息窗口对象      
		map.openInfoWindow(infoWindow13,point13); 	});	
	
	
	var RG14_PI101 = 0.5;
	var RG14_PI201 = 0.86;
	var RG14_PI101L = 0.45;
	var RG14_PI201L = 0.8;
	var RG14_PI101H = 0.55; 
	var RG14_PI201H = 0.85;
    var myIcon14 = myIcon3;
	if (RG14_PI101 > RG14_PI101H || RG14_PI201 > RG14_PI201H){myIcon14 = myIcon1;} 
	else if (RG14_PI101 < RG14_PI101L || RG14_PI201 <  RG14_PI201L){myIcon14 = myIcon2;}
	else {myIcon12 = myIcon3;}
	var point14 = new BMap.Point(124.971002,45.990271);
	var marker14 = new BMap.Marker(point14,{icon:myIcon14}); 
	map.addOverlay(marker14);             
	var label14 = new BMap.Label("哈尔宾",{offset:new BMap.Size(20,-10)});
	marker14.setLabel(label14);
	
	
	marker14.addEventListener("click", function(){   
	var RG14_PI101 = 0.5;
	var RG14_PI201 = 0.86;
	var infoWindow14 = new BMap.InfoWindow("压力：   <br />  低区压力："+RG14_PI101+" mpa <br />低区压力上限："+RG14_PI101H+" mpa <br />低区压力下限："+RG14_PI101L+" mpa <br />    高区压力："+RG14_PI201+" mpa  <br />高区压力上限："+RG14_PI201H+" mpa<br />高区压力下限："+RG14_PI201L+" mpa ",opts);  // 创建信息窗口对象      
		map.openInfoWindow(infoWindow14,point14);
	});	
	
	
	var RG15_PI101 = 1.0; 
	var RG15_PI201 = 1.5;
	var RG15_PI301 = 1.8;
	var RG15_PI101H = 1.2;
	var RG15_PI201H = 1.6;
	var RG15_PI301H = 1.9;
	var RG15_PI101L = 1.1; 
	var RG15_PI201L = 1.4;
	var RG15_PI301L = 1.7;	
       var myIcon15 = myIcon3;
	if (RG15_PI101 > RG15_PI101H || RG15_PI201 > RG15_PI201H || RG15_PI301 > RG15_PI301H){myIcon15 = myIcon1;} 
	else if (RG15_PI101 < RG15_PI101L || RG15_PI201 <  RG15_PI201L || RG15_PI301 <  RG15_PI301L){myIcon15 = myIcon2;}
	else {myIcon12 = myIcon3;}
	var point15 = new BMap.Point(104.29244,29.780948);
	var marker15 = new BMap.Marker(point15,{icon:myIcon15}); 
	map.addOverlay(marker15);            
	var label15 = new BMap.Label("成都",{offset:new BMap.Size(20,-10)});
	marker15.setLabel(label15);
	
	
	marker15.addEventListener("click", function(){   
	var RG15_PI101 = 1.0;
	var RG15_PI201 = 1.5;
	var RG15_PI301 = 1.8;
	var infoWindow15 = new BMap.InfoWindow("压力：   <br />  低区压力："+RG15_PI101+" mpa <br />低区压力上限："+RG15_PI101H+" mpa <br />低区压力下限："+RG15_PI101L+" mpa <br />    中区压力："+RG15_PI201+" mpa  <br />中区压力上限："+RG15_PI201H+" mpa<br />中区压力下限："+RG15_PI201L+" mpa<br />    高区压力："+RG15_PI301+" mpa <br />高区压力上限："+RG15_PI301H+" mpa <br />高区压力下限："+RG15_PI301L+" mpa ",opts);  // 创建信息窗口对象      
		map.openInfoWindow(infoWindow15,point15); 
	});		
	
	
	
	//添加水司坐标：RG00	
	//var myIcon00 = new BMap.Icon("images/RugaoLogo2.png", new BMap.Size(71,71));//定义图标样式	
	//var point00 = new BMap.Point(86.778213,43.9001);//定义龙泉楼坐标变量
	//var marker00 = new BMap.Marker(point00,{icon:myIcon00});  // 定义龙泉楼标注变量
	//map.addOverlay(marker00);              // 将标注添加到地图中
	//var label00 = new BMap.Label("Ecava-China:燚加华",{offset:new BMap.Size(20,-10)});//定义显示文本变量
	//marker00.setLabel(label00);//创建显示文本
	//marker00.addEventListener("click",function(e){
	//window.parent.preload.openPage("http://www.chinatelecom.com.cn/corp/lsqdcs/index.html");	});//单击打开页面		
	

	
	
	
	
	map.enableScrollWheelZoom(true);



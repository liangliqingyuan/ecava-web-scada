

var rank = +getTag( 'app.server.rank');
if (rank>0) return;

//draining animation
var pos = getTag( 'drain_valve');
var v = getTag( 'level_cook');
var d = parseFloat(getTag( 'drain'));
if (isNaN(d)) { setTag( 'drain', 0);}
if (pos == 0 && v > 0) {
    v -= 2.1;
    if ((v < 67)&&(v > 55)) { v = 54;}
    if ((v < 30)&&(v > 6)) { v = 5;}
    if (v < 0) {
        v = 0;
        setTag( 'drain_valve', 1);
    }
    if (d < 4) {
        d += 1;
    }
    setTag( 'level_cook', v);
} else {
    if (d > 0) {
        d -= 1;
    }
}
if (v == 0) {setTag( 'drain_valve', 1);}
setTag( 'drain', d);

//agitator rotation animation
var ctrstr = getTag( 'agitator_pos');
var ctr = parseFloat(ctrstr);
if (ctr < 4 && v > 0) {
    ctr++;
}
else {
    ctr = 1;
}
setTag( 'agitator_pos', ctr);


switch (ctr) {
    case 1:
    setTag( 'agitator_posA', 1);
    setTag( 'agitator_posB', 0);
    setTag( 'agitator_posC', 0);
    setTag( 'agitator_posD', 0);
        break;
    case 2:
    setTag( 'agitator_posA', 0);
    setTag( 'agitator_posB', 1);
    setTag( 'agitator_posC', 0);
    setTag( 'agitator_posD', 0);
        break;
    case 3:
    setTag( 'agitator_posA', 0);
    setTag( 'agitator_posB', 0);
    setTag( 'agitator_posC', 1);
    setTag( 'agitator_posD', 0);
        break;
    case 4:
    setTag( 'agitator_posA', 0);
    setTag( 'agitator_posB', 0);
    setTag( 'agitator_posC', 0);
    setTag( 'agitator_posD', 1);
        break;
}

//temperature simulation
var stts = getTag( 'status_cook');
var t = parseFloat(getTag( 'temperature_cook'));

if (stts > 0) {
    if (t < 80) {
        t++;
    } else {
        t = 80;
    }
} else {
    if (t > 25) {
        t--;
    } else {
        t = 25;
    }
}
setTag( 'temperature_cook', t);

//candy position/conveyor simulation
var pos = getTag( 'candy_pos');
if (pos < 100) {
    pos += 5;
}
else {
    pos = 0;
}
setTag( 'candy_pos', pos);

if (0 == getTag('kk_Demo')) {
    if (0 == stts) {
        if (0 == v) {
            setTag( 'door_position', 0);
            var level_syrup = getTag( 'level_syrup');
            var level_chocolate = getTag( 'level_chocolate');
            var level_vanilla = getTag( 'level_vanilla');
            var level_strawberry = getTag( 'level_strawberry'); 
            var recipe_syrup = getTag( 'recipe_syrup');
            var recipe_chocolate = getTag( 'recipe_chocolate');
            var recipe_vanilla = getTag( 'recipe_vanilla');
            var recipe_strawberry = getTag( 'recipe_strawberry');
            var sum = (+recipe_syrup) + (+recipe_chocolate) + (+recipe_vanilla) + (+recipe_strawberry);
            if (150 < sum) {
                setTag( 'recipe_syrup', 37.5);
                setTag( 'recipe_chocolate', 37.5);
                setTag( 'recipe_vanilla', 37.5);
                setTag( 'recipe_strawberry', 37.5);
            } else {
                var level_cook = sum+(+v);
                if (150 >= level_cook) {
                    var syrup  = level_syrup - recipe_syrup;
                    var chocolate = level_chocolate - recipe_chocolate;
                    var vanilla = level_vanilla - recipe_vanilla;
                    var strawberry = level_strawberry - recipe_strawberry;
                    if (0 > syrup) {
                        setTag('level_syrup', 99.9);
                    } else if (0 > chocolate) {
                        setTag( 'level_chocolate', 99.9);
                    } else if (0 > vanilla) {
                        setTag( 'level_vanilla', 99.9);
                    } else if (0 > strawberry) {
                        setTag( 'level_strawberry', 99.9);
                    } else {
                        setTag( 'level_syrup', syrup);
                        setTag( 'level_chocolate', chocolate);
                        setTag( 'level_vanilla', vanilla);
                        setTag( 'level_strawberry', strawberry);
                        setTag( 'level_cook', level_cook);
                        setTag( 'status_cook', 1);
                        setTag( 'door_position', 1);
                    }
                }
            }
        }
    } else if (80 <= t) {
        setTag( 'status_cook', 0);
        setTag( 'drain_valve', 0);
    }
}    
/***** Candy code end *****/
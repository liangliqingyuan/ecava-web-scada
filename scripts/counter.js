// Template for counter

if (typeof MyObject === "undefined")
	igx = {};

igx.counterTemplate = function(begin, end, increment) {
	var n = begin;
	return function() {
		var count = n;
		n += increment;
		if (n > end)
			n = begin;
		return count;
	}
}

igx.count = igx.counterTemplate(1, 86400, 1);
### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ecava IGX WEB SCADA


#### 介绍
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ecava IGX Web Scada 凭借精益架构，快速处理，增强的多功能性和专家支持，Ecava IGX 被用于自来水、污水处理、环保、石油、天然气、电力、能源、钢厂、热力、数字工厂、采矿、楼宇、电信、化工、水电站、风电站、实验检测、设备物联、水库湖泊、农业、汽车、食品、注塑硫化、路灯远控、消防、制冷等行业。Ecava IGX 在全球范围内赢得了广泛的欢迎和快速增长的客户群。

#### 软件架构
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ecava IGX Web Scada 是一个平台级的产品，其需要两个引擎的支持，1、在设计及开发时需要一个开发设计引擎；2、发布时需要运行时引擎的支持（自带WEB SERVER，无需进行环境发布、配置等一系列操作）；
###### 设计时：
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;通过C/S架构实现，通过编辑器对包括菜单的自定义（支持多级菜单）、通过平台集成的80%的通用设备协议实现数据采集的及控制、配置每个组态功能页面（兼容SVG国际标准）、报警设置、实时曲线、实时及历史报表、数据归档、集群支持、日志管理、用户及角色管理、权限设定、安全管理及打印等等，设计时采用CS架构能够最大限度的确保用户操作的高效性、便捷性。
###### 运行时：
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;通过B/S架构保证了系统多用户访问的便捷性，降低部署难度，同时采用最新的WEB技术保证了多平台间可视化的一致性，且兼容多终端设备；

![输入图片说明](%E8%BD%AF%E4%BB%B6%E6%9E%B6%E6%9E%84.PNG)

![输入图片说明](%E8%BD%AF%E4%BB%B6%E6%9E%B6%E6%9E%842.PNG)

#### 联系方式

1.  Ecava IGX WEB SCADA QQ交流群：1170455981
2.  微信：
![输入图片说明](%E5%BE%AE%E4%BF%A1.jpg)


#### 系统演示
演示系统地址如下：

演示一、http://81.70.58.107:7131/DEM0/index.html#GIS_yali.html

#### 演示案例：
###### 1、水厂监控
![输入图片说明](media/image1.png)

![输入图片说明](image2.png)

![输入图片说明](images/%E4%B8%AD%E5%9B%BD%E7%94%B5%E7%A7%91.png)

![输入图片说明](images/image4.png)

![输入图片说明](images/image5.png)

###### 2、数字化生产线监控
1、  利用3D渲染与IGX专业画图Inkscape SAGE软件结合画出工厂结构平面图，可点击页面跳转进入到相应生产线中的设备工艺画面中。在每一个生产工艺中，利用IGX下的Ip Scanner协议对设备IP进行通信监听，若设备处于通信正常状态灯会闪烁，处于断开状态灯会亮红黄。同时可显示实时时间，链接了视频流监控，可通过点击页面中摄像头图标显示实时监控画面。对于采集的数据可与现场的MES系统及其他工厂信息化系统进行数据交互，使工厂信息化高度融合。

![输入图片说明](images/image_L1.png)

     如上图，通过3D构建出数字化产线，可点击相应设备进入设备工艺图页面，点击摄像头图标可监控各设备实时工作情况。鼠标放置各个设备处可跳转至相应页面，对设备进行关键参数显示及运行状态显示。设备的运行状态显示设定了前提条件，若处于生产中，所画的颜色框会显示绿色代表运行，红色代表设备停机，黄色代表设备故障。若设备断电不显示数据则提示框将无颜色显示。

###### 3、污水处理厂监控
![输入图片说明](image_L2.png)

###### 4、锅炉厂应用：
![输入图片说明](images/image_lk3.png)
    如上图，通过给锅炉增加低成本的数据网关，实现对锅炉、水泵、水箱、节能器等设备进行实时的监控，一是用于工艺流程监控，二是可通过传感器对关键设备进行故障监测，传统的1.0设备维修方式为发现问题，对其进行维修，在当前对效率管控高的企业来说，这种维修方式是滞后的，通过对电机、锅炉等关键设备实时采集数据，基于设备的机理模型进行有效的数据分析，使故障得到提前发现，进行预防性的维护、保养，向设备维护3.0转型，能有效的降低客户损失，同时增强售后服务质量，增加客户对产品的使用粘性；

###### 5、制冷系统应用：
![输入图片说明](images/Images_88.png)
    如上图，通过SCADA实现对相关设备，如冰水箱、循环泵、大水箱、制冷机、水塔等关键设备，通过工艺流程进行整体制作，将线下的制冷工艺转换为数字化的制冷系统，使现场执行过程通过数字工艺实时的展示，除数据的采集、监控外，可通过制冷工艺要求对设备等进行工艺控制，通过设备及工艺的组合达到制冷的经济性。 

###### 6、能源管理应用：
![输入图片说明](images/image12.png)
    如上图，通过底针对能源设备及仪器的数据采集，通过平台提供的JavaScript 接口，生成能源监控大屏，针对日供水趋势、日用电趋势、供水排名、用电能排行、累计用水、用电排名进行综合分析，不需代码去实现复杂的逻辑，降低用人成本。

###### 7、智慧矿业应用：
    如下图，通过对煤矿关键设备增加具有通讯及控制功能的设备及通讯仪表，通过远程实现对煤矿泵房、液压机房、主通风系统、采煤机、掘进机、液压支臂、综掘传送带等的控制，尽量减少现场人员的使用，使煤矿的采掘更加高效。
![泵房中央控制系统](images/%E8%A5%BF%E9%A9%AC%E7%9F%BF.png)
![泵房单体泵的监视与远程控制](images/%E8%A5%BF%E9%A9%AC%E7%9F%BF2.png)
![主通风机在线监控与管控](images/%E8%A5%BF%E9%A9%AC%E7%9F%BF3.png)
![综掘带实时监控与预防性维护](images/%E8%A5%BF%E9%A9%AC%E7%9F%BF4.png)

    如下图，通过对煤矿关键设备增加具有通讯及控制功能的设备及通讯仪表，通过远程实现对煤矿泵房、液压机房、主通风系统、采煤机、掘进机、液压支臂、综掘传送带等的控制，尽量减少现场人员的使用，使煤矿的采掘更加高效。
![泵房中央控制系统](images/%E8%A5%BF%E9%A9%AC%E7%9F%BF.png)
![泵房单体泵的监视与远程控制](images/%E8%A5%BF%E9%A9%AC%E7%9F%BF2.png)
![主通风机在线监控与管控](images/%E8%A5%BF%E9%A9%AC%E7%9F%BF3.png)
![综掘带实时监控与预防性维护](images/%E8%A5%BF%E9%A9%AC%E7%9F%BF4.png)

###### 8、铁路隧道监控系统：
    How It Started
    The Malaysian Government has initiated a project for the Electrification of Double Track Project (EDTP) which runs through the northern peninsular of Malaysia. The project involves the laying and electrification of two new 329km long double track to replace the existing single track including new stations, bridges, the electrification of tracks and signalling systems. The project which started in January 2008 and scheduled to complete by 2013.

    马来西亚政府发起了一个贯穿马来西亚北部半岛的双轨电气化项目。该项目涉及铺设和电气化两条新的329公里长的双轨，以取代现有的单轨，包括新的车站、桥梁、轨道电气化和信号系统。该项目于2008年1月开始，计划于2013年完成。

    here are two double-barrel tunnels built as part of the project. These include a 3.3km long tunnel in Bukit Berapit, which is the longest in South-East Asia. This gigantic project (est. investment price RM16.5 bil (~4.1 bil USD)) is operated by KERETAPI TANAH MELAYU Berhad (KTM Berhad) Malaysia. It is the main rail operator in Peninsular Malaysia which had been running since year 1885.

    作为该项目的一部分，有两条双管隧道建成。其中包括位于布吉贝拉皮特的一条长3.3公里的隧道，这是东南亚最长的隧道。这个庞大的项目（投资价格为16.5比勒(~4.1比勒美元)）由马来西亚Keretapi Tanah Melayu Berhad(KTM Berhad)经营。它是马来西亚半岛的主要铁路运营商，自1885年开始运营。

    ![铁路隧道监控系统-马来西亚KERETAPI TANAH MELAYU有限公司（KTM Berhad）](images/image110201.png)

    It is known to be essential and compulsory to have an accurate monitoring and control system for railway tunnels, not only to ease the operators to have a convenient daily job routine, but also to ensure safety as the first priority. Therefore, in order to ensure full functionality of EDTP project, or more specifically, to provide and prepare the Berapit tunnel in the safest condition for trains to operate continually, Ecava IGX is selected as the SCADA solution to achieve these goals, and thus came out with Bukit Berapit Tunnel Monitoring and Control system (TMCS).

    众所周知，铁路隧道必须有一个准确的监控系统，这不仅是为了方便操作人员的日常工作，而且可确保安全为第一要务。因此，为了确保EDTP项目的全部功能，或者更具体地说，为了在最安全的条件下为列车持续运行提供和准备Berapit隧道，为了实现这些目标，选择了Ecava IGX作为SCADA解决方案，从而推出了Bukit Berapit隧道监控系统(TMCS)。


    Berapit Tunnel Monitoring and Control System (TMCS) by Ecava IGX

    TMCS is implemented to achieve the following goals:
    A. To monitor and control of the following systems:
       Tunnel Lighting System
       Electrical Distribution
       Tunnel Ventilation & Cross Passage Pressure Pressurization
       Hydrant System
       Telephone and Emergency Telephone system
       Public Address (PA) System
       Tunnel CCTV System
       Tunnel Access Control System
       Tunnel Radio Communication System

    A.监察及控制下列系统：
        隧道照明系统
        配电
        隧道通风与交叉通道压力加压系统
        消火栓系统
        电话及紧急电话系统
        公共广播系统
        隧道闭路电视系统
        隧道门禁系统
        隧道无线电通信系统


    B. To provide event / data logging for display and printouts on the following:
       Alarm and status monitoring of all tunnel systems and equipment
       Monitor and control of tunnel lighting
       Monitor and control of tunnel ventilation
       Monitor and control of cross passage pressurization
       Monitor and control power distribution
       Information on plant running and performance (where applicable)
       Operational procedures during emergency events
       Management data
       Tunnel access control monitoring
       Interface with other equipment (where applicable)

     B.提供事件/数据记录以显示和打印以下信息：
       所有隧道系统和设备的报警和状态监控
       隧道照明监控
       隧道通风监测和控制
       交叉通道压力监测和控制
       监控配电
       电站运行和运行情况信息(如适用)
       应急事件管理期间的操作程序
       管理数据
       隧道门禁监控
       与其他设备的监控接口(如适用的话)

      C. To store archives (in raw or CSV format) in hard disk for the purpose of extraction into external storage.
      C.将档案(原始或CSV格式)存储在硬盘中，以便提取到外部存储中。

      D. To store data up to twelve (12) months period, and perform automated house-keeping for database older than 12 months by using FIFO concept.
      D.存储长达12个月的数据，并使用先进先出的概念对12个月以上的数据库进行自动化的保管。

![The view of Bukit Berapit tunnels from outside along the railways](image110202.png)

    Ecava IGX Makes Everything Automated
Ecava IGX has equipped a TMCS which is responsible to run automatic control tunnel ventilation system in accordance with the incidents plans programmed (based on emergency and maintenance philosophy). In the meantime, it also continuously monitor the status of tunnel operations and systems.

    Ecava IGX配备了一个TMCS，负责按照制定的事故计划(基于应急和维护理念)运行自动控制隧道通风系统。同时，它还不断监测隧道运营和系统的状况。

   The security measures are even more strengthened by having Ecava IGX’s alarm automation system. Any abnormal or failure behavior will trigger alarms generation to alert operators, and they will be automatically recorded and can also be printed or exported.

   通过拥有Ecava  IGX的报警自动化系统，安全措施得到了更大的加强。任何异常或故障行为都将触发警报生成，以提醒操作员，并将自动记录，也可打印或导出。

   TMCS operates to provide comprehensive management information in the form of historical trends / report, with the purpose to ensure the most accurate assessment of tunnel operating costs which leads to top-level efficiency.

   隧道管理系统以历史趋势/报告的形式提供全面的管理信息，目的是确保对隧道运营成本进行最准确的评估，从而达到最高水平的效率。

   Manual control facilities for tunnel lighting and ventilation are executable from user workstations in accordance with the incidents plans philosophy.

   隧道照明和通风的手动控制设施可从用户工作站根据事故计划的理念执行。

   TMCS provides the operational interface for the surveillance and control of the tunnels. There are four (4) TMCS servers installed in multiple cross-states technical control buildings, including one in the Kuala Lumpur central control center. TMCS is designed to allow multiple users access for 24 hours operation purpose by following the best security approaches. Thus, there are three (3) TMCS workstations furnished among those control buildings, which each of them has the same operation interface and functions as TMCS server.

   隧道监控系统为隧道的监控提供了操作界面。有四台TMCS服务器安装在多个跨州技术控制大楼中，包括吉隆坡中央控制中心的一台。TMCS旨在通过遵循最佳的安全方法，允许多个用户24小时访问。因此，在这些控制建筑物中有三个TMCS工作站，它们各自具有与TMCS服务器相同的操作接口和功能。

   In normal operation, among the four (4) TMCS servers in same network connection, one of them will be promoted as Master to control most of the monitoring, controlling, redundancy and data archiving work. While the rest will act as standby TMCS server. This redundancy is yet another approach to ensure the system availability for all time.

   在正常运行中，在同一网络连接的四台TMCS服务器中，其中一台将被提升为主控服务器，控制大部分的监控、冗余和数据归档工作。其余的将充当备用TMCS服务器。这种冗余是确保系统始终可用的另一种方法。
![Operator can access TMCS via TMCS servers or workstations in same network connection.](image110203.png)

   The Architecture
   The TMCS is designed based on the use of remote input output units and everything is interconnected via Ethernet TCP/IP private network. All of the system clocks for Operating Systems reside in Servers, Workstations and PLC are synchronized by a Master Clock via Network Time Protocol (NTP).

   TMCS是基于远程输入输出单元的使用而设计的，通过以太网TCP/IP专用网络实现万物互联。操作系统的所有系统时钟都驻留在服务器，工作站和PLC上，由主时钟通过网络时间协议(NTP)进行同步。

   The TMCS is built as a distributed system with TMCS PLC panels mounted within the tunnel cross passages, and technical control buildings are interconnected by a private LAN (TMCS Network) to the TMCS Servers as well as interfacing with TMCS Workstations.

   TMCS是一个分布式系统，具有TMCS安装在隧道交叉通道内的PLC面板，与技术控制大楼通过专用局域网(TMCS Network)连接到TMCS服务器，并与TMCS工作站连接。

   This system hierarchy is designed to allow local autonomous operation from different control buildings, to ensure the TMCS can be operated despite any communication failure in any of the control rooms.

   该系统层次结构旨在允许来自不同控制建筑的本地自主操作，以确保TMCS可以在任何控制室中通信故障的情况下运行。

   The TMCS provides a safe, secure and comprehensive tunnel management system that requires minimum staffing level.

   隧道监控系统提供一个安全、可靠和全面的隧道管理系统，需要最低限度的人员配置。

![Bukit Berapit tunnel ventilation fallback panel](image110204.png)

   The Hardware / Software:
   Server Machine: Dell Power Edge 710 Server
   OS: Windows Server 2008 R2 Service Pack 1

   Workstation Machine: Dell Precession T3500
   OS: Windows 7 Professional Service Pack 1PLC (Programmable Logic Controllers): GE Fanuc type RX3i

   Protocol: Modbus TCP/IP

   SCADA: Ecava IGX
   IO tags: 8192 tags
   Redundancy: 4 servers
   Remote client: 7 clients

   Database: PostgreSQL Database

 **IGX Web SCADA Mimics Screenshots:** 
   TMCS by Ecava IGX has been completed and fully implemented back in year 2013 and is currently serves as the strong backbone to support the Berapit tunnels, and ensuring all-time uninterrupted train operation.

![TMCS Overall View Mimic](image110205.png)

![TMCS Ventilation Fallback Panel Mimic](image110206.png)

###### 9、配电网络系统：

实施主体：（马来西亚海洋和重工程控股有限公司（MHB））

Ecava IGX Takes Care of Power Distribution for MHB
Ecava IGX负责MHB的电力分配

How It Started

   Malaysia Marine and Heavy Engineering Holdings Berhad (MHB), or formerly known as MMHE, is a leading offshore and marine services provider in Malaysia, focused primarily on the oil and gas sector. Operations across the MHB yards located in Pasir Gudang (Johor, Malaysia) are supported and supplied by the power distribution. These extremely high voltage powers are monitored and controlled by the professional team in a power facility room located in the administration building of MHB. There are rows and rows of panels comprising with numbers of power meters in the control room area, in which the operators are required to walk to each of them in order to monitor the entire power distribution. Imagine if there is an emergency situation, it is very important for the operator to be notified at the earliest moment to control any unwanted loss, but it could be difficult to achieve by their manual monitoring work routine. Therefore, MHB has chosen Ecava IGX SCADA to build an accurate and automated monitoring system to guard the power plant operation.

   马来西亚船舶和重型工程控股公司Berhad(MHB)是马来西亚领先的离岸和海上服务供应商，主要专注于石油和天然气行业。MHB跨区域运营位于Pasir Gudang(马来西亚Johor)的船厂由配电系统支持和供应。这些极高的电压功率由专业团队在位于MHB行政大楼的电力设施室中进行监测和控制。控制室区域内有一排排带有电能表数目的面板，其中要求操作者步行到每一组，以监测整个电力分配情况。试想一下，如果出现紧急情况，尽早通知经营者以控制不必要的损失是非常重要的，但他们的人工监测工作很难做到。因此，MHB选择了Ecava IGX SCADA来建立一个准确、自动化的监控系统，以保障电厂的运行。

   ![One of the many rows of power meter panels in MHB power facility room](image110207.png)

   MHB动力设备室中的多排电表面板之一

   ![MHB动力设备室中的多排电表面板之一](image110208.png)

   Operator had to monitor each panel manually everytime during the job routine schedule. Power Distribution Network by Ecava IGX provided the solution to monitor the system in the efficient way.

   在日常作业计划中，操作员每次都要手动监控每个面板。Ecava IGX公司的配电网络为系统的高效监控提供了解决方案。

   Power Distribution Network (PDN) System by Ecava IGX  
   基于ECARA IGX的配电网(PDN)系统

   In this project, Ecava IGX is required to monitor the power meter readings accurately. The Power Distribution Network (PDN) system is designed to achieve this by having the best connection architecture in order to yield the efficiency. All the digital power meters will be transmitting serial data via Modbus RTU protocol to MOXA MGate serial converter. These serial data will be converted to digital data by MOXA MGate, then send to Ecava IGX SCADA server via Modbus TCP/IP protocol. PDN system will process these data to present them on mimic screens to ease operators’ routine work, as well as record them as real-time / historical trends and reports.

    在这个项目中，Ecava IGX需要对电能表的读数进行准确的监测。电力分配网络(PDN)系统就是为了达到这一目的而设计的，它采用了最佳的连接结构，从而提高了系统的效率。所有的数字电能表都将通过Modbus RTU协议传输串行数据到MOXA MGate串行转换器。这些串行数据将通过MOXA MGate转换成数字数据，然后通过Modbus TCP/IP协议发送到Ecava IGX SCADA服务器。PDN系统将处理这些数据，将其显示在模拟屏幕上，以方便操作者的日常工作，并将其记录为实时/历史趋势和报告。

![PDN系统的基本体系结构](image110209.png)

   The basic system architecture for PDN system
   PDN系统的基本体系结构

   Ecava IGX Emerges to Make Life Easier in Power Facility Room
   Ecava IGX的出现使电力设施室的生活更加轻松

   PLC (Programmable Logic Controllers) is equipped in this project to aid the control operation of switchgear for PDN. SCADA mimics are designed imitate the hard button switchgear panel in an identical way, the reason is to target operators’ convenience in familiarize with the automated PDN system. Operators can initiate requests on the panel mimic screen easily to control the switchgear operation.

   本项目采用可编程控制器(PLC)辅助PDN开关柜的控制操作。SCADA模拟系统是以相同的方式模拟硬按钮开关面板柜，目的是为了方便操作人员熟悉自动化PDN系统。操作人员可以在面板模拟屏幕上简单地发起请求，以控制开关柜的操作。

![PDN系统的SCADA仿真是为了模拟开关柜而设计的。](image110210.png)

   SCADA mimic for PDN system is designed to imitate the switchgear panel
   PDN系统的SCADA仿真是为了模拟开关柜而设计的。

    Other than that, since there are different voltages across the entire power distribution, different color codes are used to design the mimics. The approach shall lead to easier inspection whenever it is required.

    除此之外，由于整个功率分布都有不同的电压，所以使用不同的色码来设计模拟电路。这种方法应在需要时使检查更容易。

![PDN系统的SCADA模拟设计采用不同的色线图解来表示不同的电压。](image110211.png)
    SCADA mimic for PDN system is designed with different color lines illustration to indicate different voltage

    PDN系统的SCADA模拟设计采用不同的色线图解来表示不同的电压。

The Hardware / Software:
Server Machine: DELL PowerEdge R720 Server
OS: Windows Server 2012 R2
Device: Digital Power Meter
Schneider Electric PowerLogic PM850
Rudolf DPA96A
Atec MDM3100
Protocol: Modbus RTU (to MOXA MGate serial converter)
Protocol: Modbus TCP (from MOXA MGate serial converter to Ecava IGX SCADA server)

PLC (Programmable Logic Controllers): Siemens S7-1200
Protocol: Profinet
SCADA: Ecava IGX
IO tags: 8192 tags

Database: PostgreSQL database

 **Gallery** 

![The closeup of one of the digital power meters (Rudolf DPA96A)](image110213.png)
   The closeup of one of the digital power meters (Rudolf DPA96A)
PDN system mimic for one group of power meter reading
Data from digital power meter will be collected and plotted into trends in PDN system

![输入图片说明](image110214.png)

![输入图片说明](image110216.png)




#### 关于Ecava IGX Web SCADA平台
IGX Web SCADA将底层的通信协议（市场80%以上的硬件通信协议）进行底层封装并提供出相应的接口，同时将获取到的数据打包成标准的JS库，对于开发人员则更加关注系统本身的核心业务逻辑，拿到采集到的数据进入哪个流程环节，这是我们需要重点关注的，对于采集层来说，无论通过哪种方式获取到的数据都是一致的，所以我们将获取到的数据或反向控制的点位的方式进行打包并将属性及该当开放给使用人员，开发人员无须关注数据采集的过程，专注业务逻辑本身，这是企业主所关注的价值点。

#### 关于ECAVA IGX 二次开发
只要开发人员熟悉JS库，即可将市面流行的各种JS库引用到IGX编辑器中，进行二次开发，像THREEJS等等，我们定位IGX为一款基础的物联网开发平台，可以像使用各种前端开发工具编辑对应的JS及HTML文件等，同时做为物联网基础底座，系统提供标准的API及其他接口对接现有的其他信息化系统对接，对于企业来说属于一次性的付出永久的收益，帮助企业实现数字化转型。
后期将逐步发布相应的二次开发相关资料，请大家持续关注；

#### 安装教程

1.  前往联系方式章节中指定的QQ群中下载对应版本的SCADA平台；
2.  下载对应版本的SVG编辑器；

#### 使用说明

   
###### 1. 第一章 简介
 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ecava IntegraXor（IGX）是具有图形动画，实时设备连接，报警功能，数据库记录，趋势和报告作为基本功能的HMI / SCADA软件。Ecava IGX是HTML5 + CSS3兼容的全功能网络监控软件。一个真正的Web SCADA软件，可运行在各种平台浏览器上！它使用纯Web技术设计，以创建一个完整的工具，用于构建复杂和智能的SCADA系统。
   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;一个功能完备的SCADA系统，令人惊讶的简化， 在不到1分钟内下载并安装。 传统SCADA系统难以安装和设置，需要技术性非常强的工程师进行复杂的配置。IGX从概念上挑战传统SCADA系统，具基于用户友好的角度构建的，所有U.I 具有自我解释与安全功能。现在体验如何容易地掌握新一代的SCADA系统！
    
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ecava IGX在全世界50多个国家稳定运行！三十人研发团队经过数十年的不断的升级改进，经过专业的严格极限测试，确保为不同行业提供最稳定、最安全、最好用、最优质的服务，为我们大大节省了时间和金钱，并极大地提高了效率。

###### &nbsp;&nbsp;&nbsp;&nbsp;目的

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本文档的目的是向初学用户说明如何开始使用Ecava IGX根据自己的需求快速创建和实施项目。此外，作为一个真正的网络产品，Ecava IGX利用HTML和JavaScript作为编程语言。 如果你不熟悉任何一种语言，不要担心。 我们将向您展示如何经过简单的操作来完成专业的、功能强大的WEB SCADA系统。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;在开始之前，请确保已安装以下应用程序。
     1）Ecava IGX SCADA
     2）图形编辑器Inkscape SAGE

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;如果您没有上述所有程序，请通过联系方式一章节进行索取，下载并安装在您的电脑上。
###### &nbsp;&nbsp;&nbsp;&nbsp;系统要求

|   事项   |       内容                                                       |
|----------|---------------------------------------------------------------------------------------|
| 操作系统      | Microsoft Windows XP, 或以上                                                            |
| 处理器      | Intel © Core ™ 2 @ 1.66GHz                                                            |
| 硬盘空间     | ~25MB 用于运行 ~200MB 用于开发                                                                |
| 内存       | 3GB                                                                                   |
| 运行时(浏览器) | Mozilla Firefox 3.5+, Google Chrome 3.0+, or Microsoft Internet Explorer 8+ (安装SVG插件) |
 

###### &nbsp;&nbsp;&nbsp;&nbsp;工业自动化基础
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;如果您以前使用过HMI / SCADA应用程序或PLC，则可以跳过下一章。 如果您刚接触SCADA或工业自动化，您将需要一些基本的工业自动化，通信协议和标签/点的概念，您将在下一章学习。

###### 2. 第二章 基本知识
###### &nbsp;&nbsp;&nbsp;&nbsp;可编程逻辑控制器（PLC）
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;可编程逻辑控制器（PLC）是用于自动化过程的基于微处理器的装置，例如对工厂装配线上的机械的控制，或者控制伺服机和输送线。 PLC的一个关键特性是连接到传感器和执行器的输入/输出（I / O）设备。通过这些I / O，PLC可以读取限位开关，模拟过程变量（如温度和压力）和复杂定位系统的位置。 PLC还可以操作电动机，磁继电器或电磁阀，气动或液压缸以及模拟输出。

###### &nbsp;&nbsp;&nbsp;&nbsp;数据通信
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PLC内部有大量信息。诸如数学计算或设备的输入状态的信息存储在PLC的数据区中。数据区是PLC的内部寄存器，每个都有自己的存储器地址。这些数据可通过内置在PLC中的通讯端口从外部系统访问。通常，PLC具有Modbus、DeviceNet或Profinet等协议以及以太网端口和各种现场总线。例如，电机的运行状态可通过输入1提供给PLC。根据PLC的状态，输入1的寄存器地址可映射到Modbus地址10001。

###### &nbsp;&nbsp;&nbsp;&nbsp;PLC＆Ecava IGX
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ecava IGX是开发HMI / SCADA应用程序的工具。 Ecava IGX提供通用通信驱动程序，通过其通信端口与PLC直接交换数据。在上述示例中，为了将电机运行状态读入Ecava IGX，我们需要创建一个端口，在Ecava IGX项目编辑器中提供PLC配置，并创建一个对应于Modbus地址10001的数字I / O标签。Ecava IGX服务器将通过通信端口轮询PLC，并用实时信息更新标签。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;除了PLC，Ecava IGX还可以与各种其他设备通信，如支持常用工业通信协议的机器人和驱动器。
Ecava IGX还提供了为用户绘制图形用户界面的工具。例如，一个简单的罐体可以由一个矩形表示，并随着水位的上升和下降而动画化。可以使用Ecava IGX配置报警，以便早期检测和警告。 

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ecava IGX还提供了为用户绘制图形用户界面的工具。例如，一个简单的罐体可以由一个矩形表示，并随着水位的上升和下降而动画化。可以使用Ecava IGX配置报警，以便早期检测和警告。 Ecava IGX中还提供了数据库日志记录，趋势和许多其他工具。 Ecava IGX的灵活性和现代网络技术的使用为您的全球自动化系统提供了无限的可能性。

![输入图片说明](%E7%AC%AC%E4%B8%80%E3%80%81%E4%BA%8C%E7%AB%A0%EF%BC%9A%E7%AE%80%E4%BB%8B%E5%8F%8A%E5%9F%BA%E6%9C%AC%E7%9F%A5%E8%AF%86.jpg)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;图2.1：Ecava IGX基本网络架构

下面的屏幕截图显示了使用Ecava IGX开发的项目主页：

![输入图片说明](%E7%AC%AC%E4%B8%80%E3%80%81%E4%BA%8C%E7%AB%A0%EF%BC%9A%E7%AE%80%E4%BB%8B%E5%8F%8A%E5%9F%BA%E6%9C%AC%E7%9F%A5%E8%AF%862.jpg)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;图2.2 Ecava IGX项目屏幕截图（工厂自动化）


###### 3. 第三章 认识Ecava IGX Web SCADA系统
###### &nbsp;&nbsp;&nbsp;&nbsp;软件下载
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;下载软件 1、Ecava IGX SCADA；2、图形编辑器Inkscape SAGE。
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;来源: QQ交流群共享文件：1170455981
###### &nbsp;&nbsp;&nbsp;&nbsp;一：1、Ecava IGX SCADA软件安装
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;下载解压后WINXP双击Ecava-IGX.CN-SCADA-6.0.321.2-32bit-Beta.msi运行程序。（win7、win8右键“安装”或“以管理员身份安装”；win10及上操作系统用户打开任务管理器→文件→运行新任务→勾选“以系统管理员权限创建此任务” →浏览“所有文件”找到软件→确定）。如图：

![安装-新建任务](%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E8%AE%A4%E8%AF%86Ecava%20IGX%20Web%20SCADA%E7%B3%BB%E7%BB%9F.jpg)

勾选“我接受许可协议中的条款” →下一步

![安装-同意安装协议](%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E8%AE%A4%E8%AF%86Ecava%20IGX%20Web%20SCADA%E7%B3%BB%E7%BB%9F2.jpg)

自由选择安装用户，默认为所有用户→下一步

![输入图片说明](%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E8%AE%A4%E8%AF%86Ecava%20IGX%20Web%20SCADA%E7%B3%BB%E7%BB%9F3.jpg)

选择安装位置或默认，根据自己意愿可勾取掉“自动发送错误”→下一步

![输入图片说明](%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E8%AE%A4%E8%AF%86Ecava%20IGX%20Web%20SCADA%E7%B3%BB%E7%BB%9F4.jpg)

选择语言（默认为简体中文）→安装

![输入图片说明](%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E8%AE%A4%E8%AF%86Ecava%20IGX%20Web%20SCADA%E7%B3%BB%E7%BB%9F5.jpg)

等待20秒左右→完成（即可看到DEMO）

###### &nbsp;&nbsp;&nbsp;&nbsp;2、Ecava IGX SCADA软件组成
安装完成后可在“所有程序”中看到软件，由编辑软件![输入图片说明](%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E8%AE%A4%E8%AF%86Ecava%20IGX%20Web%20SCADA%E7%B3%BB%E7%BB%9F6.jpg)和运行软件![输入图片说明](%E7%AC%AC%E4%B8%89%E7%AB%A0%EF%BC%9A%E8%AE%A4%E8%AF%86Ecava%20IGX%20Web%20SCADA%E7%B3%BB%E7%BB%9F7.jpg)软件组成；编辑软件“IGX SCADA Project Editor”软件完全免费，是用来组态开发的工具，教程中简称“PE”;运行软件“IGX SCADA Server”是项目WEB运行服务器软件，用来运行项目，可免费使用2小时，每次只需重启软件即可再次获得2小时免费运行，直到正式注册。因此用户可以完全开发好自己的项目，测试所有功能后再购买授权，避免给工程师造成各种压力。
###### &nbsp;&nbsp;&nbsp;&nbsp;3、IGX SCADA Project Editor（或“PE”）软件认识双击“PE”：

![输入图片说明](A2.jpg)
![在图标1或图标2处打DEMO文件](A1.jpg)

如下图打开PE软件后，主要有4个区域，可开发出所有所需功能的WEB SCADA项目。
![输入图片说明](A3.jpg)
点击PE软件中的图标5“运行项目”。可自动启动“IGX SCADA Server”并且运行DEMO项目。

###### &nbsp;&nbsp;&nbsp;&nbsp;4、IGX SCADA Server软件
由PE运行或双击![输入图片说明](A4.jpg)图标运行如上述步骤打开DEMO文件。
![输入图片说明](A5.jpg)
如上图4图标点击“启动项目”，即可以浏览器为客户端工具访问到服务器。
按“F11”可全屏，更好展显DEMO，DEMO里的项目为真实案例而来。

###### &nbsp;&nbsp;&nbsp;&nbsp;5、客户端浏览
如图全屏浏览DEMO服务器，可将浏览器地址复制到局域网其它PC浏览器，或手机扫描二维码。
![输入图片说明](A6.jpg)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;工厂自动化
![输入图片说明](A7.jpg)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;楼宇自动化
![输入图片说明](A8.jpg)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;水利自动化
![输入图片说明](A9.jpg)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;热力锅炉自动化
![输入图片说明](A10.jpg)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;水网自动化
![输入图片说明](A11.jpg)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;楼宇自动化
![输入图片说明](A12.jpg)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电力自动化
![输入图片说明](A13.jpg)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;水处理自动化
二：1、图形编辑器Inkscape SAGE安装
Ecava IGX软件画面以功能强大的Inkscape SAGE软件开发。
双击或右键“以管理员权限运行” inkscape091_sage416.exe软件。默认→OK

![输入图片说明](1.jpg)

下一步

![输入图片说明](2.jpg)

下一步（由于字库原因，不影响）

![输入图片说明](3.jpg)

默认→下一步

![输入图片说明](4.jpg)

选择安装路径或默认→安装

![输入图片说明](5.jpg)
![输入图片说明](6.jpg)

等待一会，安装完成后点“下一步”

![输入图片说明](7.jpg)

点击“完成”运行软件。
![输入图片说明](8.jpg)

2、在PE软件中组态画面

双击PE中“屏幕”中的“Plant Prlsess”打开Inkscape SAGE软件可修改画面。
![输入图片说明](9.jpg)

尝试下鼠标滚珠滑动及与Ctrl 和Shift键组合应用，调整画面位置。
![输入图片说明](10.jpg)

3、设置动画属性及链接标签
右键任何图形点击“对象属性”可设置图形动画原则或链接标签。
![输入图片说明](11.jpg)

按住Ctrl键单击可选择群组内图形。如图显示值的属性

![输入图片说明](12.jpg)

点击如图按钮可打开标签链接对话框

![输入图片说明](13.jpg)

可通过按钮查看DEMO项目中所有标签，选择后点”OK”即可实现此值显示功能。

![输入图片说明](14.jpg)


###### 4. 第四章 新建项目
###### &nbsp;&nbsp;&nbsp;&nbsp;新建项目
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单击开始>程序> Ecava IGX> IntegraXor编辑器（或在桌面双击图标![输入图片说明](images/image12.png)）。打开Ecava IGX项目编辑器或简称PE，您将在其中配置SCADA项目。
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;语言设置：工具→介质语言→中文（简体）：仅首次设置一次。如下图：
![输入图片说明](images/image1101.png)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;第一步：文件→新项目（或Ctrl+N）打开创建项目对话框。
![输入图片说明](images/image110102.png)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;第二步：输入项目名称：Candy→项目存储位置（可默认）→确定
![输入图片说明](images/image110103.png)

这时PE已经为Candy项目建立了默认的模板，初学者只需对这些模板进行少量修改，就会得到一个功能完事的项目。

 **提示：项目是以文件夹的形式存放，当项目开发完，只需将此文件夹整体拷贝到运行服务器即可。** 

![输入图片说明](images/image110104.png)

######  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**1、菜单、工具区：文件菜单：** 创建、保存项目等，
        编辑菜单：复制、粘贴等，
        视图菜单：打开关闭显示区窗口
        工具菜单：导入、导出、语言等
        帮助菜单：帮助文件、授权、版本说明等

###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**2、文件任务区：** 文件窗口包含项目所有配置文件；任务窗口包含项目常规，定时器，标记，数据库，安全，报警，脚本，屏幕，报告等；可点击下方“文件”、“任务”进行切换，如下图：

![输入图片说明](images/image110105.png)

可通过上方按钮关闭、隐藏，如下图：

![输入图片说明](images/image110106.png)

关闭后可通过视图菜单“文件窗口”、“任务窗口”恢复或通过快捷键Ctrl+Shit+T、Ctrl+Shit+F操作。

######  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**3、主编辑区：** 点击左侧文件或任务，打开对应的编辑窗口。
######  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**4、动态帮助区：** 可通过点击工具栏“动态帮助”按钮或“F1”快捷键打开或关闭。
######  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**5、信息、错误提示、交互区：** 用于程序编辑过程中的各类提示。可通过视图菜单、窗口按钮或快捷键快捷键Ctrl+Shit+R、Ctrl+Shit+U进行操作。

###### &nbsp;&nbsp;&nbsp;&nbsp;第三步：检查新建项目一般属性：任务窗口Candy→概要

![输入图片说明](images/image110107.png)

######  &nbsp;&nbsp;&nbsp;&nbsp;**第四步：运行服务器：** 点击PE工具栏![输入图片说明](images/image110108.png)或快捷键Ctrl+F6，Ecava IGX Server将自动运行，同时打开浏览器客户端。

注意：它需要管理员权限才能运行。 如果在PC上启用用户帐户控制（UAC），将弹出一条消息，询问您是否允许运行Ecava IGX Server。 您必须选择“是”才能运行服务器。

![图4.4：Ecava IntegraXor服务器](images/image1109.png)

 **提示：** 服务器显示Ecava IGX在后台运行的任务以及详细信息。 此信息在监视任务、故障排除或诊断错误时非常有用。
然后Internet Explorer打开包含URL http：//localhost：7131/Candy /的以下页面。 您可能已经注意到Web浏览器的自动启动是在一般项下的后启动中配置，运行后界面如下图：

![图4.5：Internet Explorer上的IntegraXor项目主页](images/image1110.png)

![图4.6：Internet Explorer上的IntegraXor实时曲线报表](images/image1111.png)

![图4.7：IntegraXor实时报警信息](images/image111201.png)

![图4.8：IntegraXor实时采集数据展示报表](images/image111202.png)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;内置的Web服务器在Ecava IGX Server中提供诸如index.html之类的页面。它默认使用端口7131。如果本地网络中有多个计算设备，请在其他计算机或同一网络中的智能手机上使用浏览器查看项目。也扫描二维码。在浏览器中输入相应的项目URL。例如，如果Ecava IGX Server在IP地址为192.168.1.100的计算机上运行，并正在侦听端口7131，则URL为http://192.168.1.100:7131/Candy/。


###### 5. 第五章 设备配置

###### 6. 第六章 监控画面制作（一）

###### 7. 第六章 监控画面制作（二）

###### 8. 第七章 报警功能快速配置

###### 9. 第八章 曲线（曲势）功能快速配置

###### 10. 数据库（Postgre SQL）快速配置

###### 11. 页面管理

###### 12. 用户权限安全功能快速配置

###### 13. 语言设置

###### 14. 总结

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

function getProcessData( prev, range) {
    var rnd = Math.random()*5;
	if ((prev + rnd)> range) {
	    rnd = prev - rnd;
	}
	else {
	    rnd = prev + rnd;
    }
    return rnd;
}

var data = getProcessData( getTag("pressure_cook"), 80);
setTag("pressure_cook", data);